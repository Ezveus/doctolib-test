require 'test_helper'

class EventTest < ActiveSupport::TestCase
    test "one simple test example" do
        Event.create kind: 'opening', starts_at: DateTime.parse("2014-08-04 09:30"), ends_at: DateTime.parse("2014-08-04 12:30"), weekly_recurring: true
        Event.create kind: 'appointment', starts_at: DateTime.parse("2014-08-11 10:30"), ends_at: DateTime.parse("2014-08-11 11:30")

        availabilities = Event.availabilities DateTime.parse("2014-08-10")
        assert_equal Date.new(2014, 8, 10), availabilities[0][:date]
        assert_equal [], availabilities[0][:slots]
        assert_equal Date.new(2014, 8, 11), availabilities[1][:date]
        assert_equal ["9:30", "10:00", "11:30", "12:00"], availabilities[1][:slots]
        assert_equal [], availabilities[2][:slots]
        assert_equal Date.new(2014, 8, 16), availabilities[6][:date]
        assert_equal 7, availabilities.length
    end

    test "a complexer case" do
        Event.create kind: 'opening', starts_at: DateTime.parse("2014-08-06 10:00"), ends_at: DateTime.parse("2014-08-06 13:30"), weekly_recurring: true
        Event.create kind: 'opening', starts_at: DateTime.parse("2014-08-10 11:00"), ends_at: DateTime.parse("2014-08-10 12:00")
        Event.create kind: 'appointment', starts_at: DateTime.parse("2014-08-10 11:00"), ends_at: DateTime.parse("2014-08-10 11:30")
        Event.create kind: 'appointment', starts_at: DateTime.parse("2014-08-13 12:30"), ends_at: DateTime.parse("2014-08-13 14:00")

        availabilities = Event.availabilities DateTime.parse("2014-08-08")
        assert_equal Date.new(2014, 8, 8), availabilities[0][:date]
        assert_equal [], availabilities[0][:slots]
        assert_equal Date.new(2014, 8, 10), availabilities[2][:date]
        assert_equal ["11:30"], availabilities[2][:slots]
        assert_equal Date.new(2014, 8, 14), availabilities[6][:date]
        assert_equal [], availabilities[6][:slots]
        assert_equal Date.new(2014, 8, 13), availabilities[5][:date]
        assert_equal ["10:00", "10:30", "11:00", "11:30", "12:00"], availabilities[5][:slots]
        assert_equal 7, availabilities.length
    end

    test "an event can not end before it begins" do
        assert_raises ActiveRecord::RecordInvalid do
            Event.create! kind: 'opening', starts_at: DateTime.parse("2014-08-06 10:00"), ends_at: DateTime.parse("2014-08-04 13:30"), weekly_recurring: true
        end
    end

    test "an appointment can not be recurring" do
        assert_raises ActiveRecord::RecordInvalid do
            Event.create! kind: 'appointment', starts_at: DateTime.parse("2014-08-06 10:00"), ends_at: DateTime.parse("2014-08-06 13:30"), weekly_recurring: true
        end
    end

    test "appointment and opening are the only allowed kinds of events" do
        assert_raises ActiveRecord::RecordInvalid do
            Event.create! kind: 'another', starts_at: DateTime.parse("2014-08-06 10:00"), ends_at: DateTime.parse("2014-08-06 13:30")
        end
    end

    test "events can span only on one day" do
        assert_raises ActiveRecord::RecordInvalid do
            Event.create! kind: 'opening', starts_at: DateTime.parse("2014-08-06 10:00"), ends_at: DateTime.parse("2014-08-07 10:00")
        end
    end

    test "appointments_slots_for_date works" do
        Event.create kind: 'appointment', starts_at: DateTime.parse("2014-08-13 12:30"), ends_at: DateTime.parse("2014-08-13 14:00")

        assert_equal [], Event.appointments_slots_for_date( DateTime.parse("2014-08-08") )
        assert_equal ["12:30", "13:00", "13:30"], Event.appointments_slots_for_date( DateTime.parse("2014-08-13") )
    end

    test "openings_slots_for_date (non-recurring) works" do
        Event.create kind: 'opening', starts_at: DateTime.parse("2014-08-10 09:00"), ends_at: DateTime.parse("2014-08-10 12:00")

        assert_equal [], Event.openings_slots_for_date( DateTime.parse("2014-08-08") )
        assert_equal ["9:00", "9:30", "10:00", "10:30", "11:00", "11:30"], Event.openings_slots_for_date( DateTime.parse("2014-08-10") )
    end

    test "openings_slots_for_date (recurring) works" do
        Event.create kind: 'opening', starts_at: DateTime.parse("2014-08-10 09:00"), ends_at: DateTime.parse("2014-08-10 12:00"), weekly_recurring: true

        assert_equal [], Event.openings_slots_for_date( DateTime.parse("2014-08-13") )
        assert_equal ["9:00", "9:30", "10:00", "10:30", "11:00", "11:30"], Event.openings_slots_for_date( DateTime.parse("2014-08-10") )
        assert_equal ["9:00", "9:30", "10:00", "10:30", "11:00", "11:30"], Event.openings_slots_for_date( DateTime.parse("2014-08-17") )
    end
end
