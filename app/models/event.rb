class Event < ApplicationRecord
    VALID_KINDS = ['appointment', 'opening']

    validate do |event|
        errors.add( :invalid_dates, "Event dates are invalid" ) if event.starts_at > event.ends_at
        errors.add( :recurring_appointment, "An appointment can not be recurring" ) if event.kind == 'appointment' && event.weekly_recurring
        errors.add( :invalid_time_range, "Event dates span over more than one day" ) if event.starts_at.to_date != event.ends_at.to_date
    end

    validates_each :kind do |record, attr, value|
        record.errors.add( attr, "must be one of: #{VALID_KINDS.join( ', ' )}" ) unless VALID_KINDS.include? value
    end

    def slots
        (self.starts_at.to_datetime.to_i ... self.ends_at.to_datetime.to_i).step(30.minutes).collect do |timestamp|
            Time.at( timestamp ).gmtime.strftime( "%-H:%M" )
        end
    end

    def self.appointments_slots_for_date( date )
        self.where( kind: 'appointment', starts_at: (date.to_date ... (date.to_date + 1) ) ).collect( &:slots ).flatten.uniq
    end

    def self.openings_slots_for_date( date )
        slots_for_simple_openings = self.where( kind: 'opening', weekly_recurring: nil, starts_at: (date.to_date ... (date.to_date + 1) ) ).collect( &:slots ).flatten.uniq

        slots_for_recurring_openings = self.where( "kind = 'opening' AND weekly_recurring = :weekly_recurring AND (starts_at < :date OR (starts_at >= :date AND starts_at < :end_date))",
            {weekly_recurring: true, date: date.to_date, end_date: date.to_date + 1} ).select { |event|
            # We use directly the numerator since substracting date from date means denominator IS 1
            closest_date = event.starts_at.to_date + 7 * ((date.to_date - event.starts_at.to_date).numerator / 7)

            closest_date == date
        }.collect( &:slots ).flatten.uniq

        slots_for_simple_openings + slots_for_recurring_openings
    end

    def self.availabilities( start_date )
        (start_date.to_date ... (start_date.to_date + 7)).collect do |date|
            opening_slots = openings_slots_for_date( date )
            appointment_slots = appointments_slots_for_date( date )

            opening_slots -= appointment_slots unless appointment_slots.nil?

            {date: date, slots: opening_slots}
        end
    end
end
